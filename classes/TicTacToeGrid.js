class TicTacToeGrid extends GenericGrid {
    constructor(options){
        super(options)
    }
    createCell (rowIndex, colIndex){
        return new TicTacToeCell ({
            rowIndex, 
            colIndex, 
            width: this.cellWidth, 
            height: this.cellHeight
        })
    }
}